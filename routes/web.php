<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\ContractsController;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Redirect::route('home');
});

Auth::routes();

\Illuminate\Support\Facades\Broadcast::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('key-auth/{key}', [LoginController::class, 'loginByTaskKey'])->name('auth-by-key');

Route::middleware(['auth'])->group(function () {
    Route::resource('tasks', TaskController::class);
    Route::resource('task/{task}/contracts', ContractsController::class)->only(['create', 'show', 'update']);
    Route::post('/contracts/{contract}/start', [App\Http\Controllers\ContractsController::class, 'start'])->name('contracts.start');
    Route::post('/contracts/{contract}/pay', [App\Http\Controllers\ContractsController::class, 'pay'])->name('contracts.pay');

    Route::get('/attachments/{id}', [App\Http\Controllers\ChatAttachmentsController::class, 'show'])->name('attachments.show');

});
